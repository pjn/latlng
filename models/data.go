package models

import (
	"bufio"
	"encoding/csv"
	"io"
	"log"
	"os"
	"strconv"
	"time"
	"latlng/types"
)

// GetData - returns all data from CSV file
func GetData(file string) (points []types.Point, err error) {

	csvFile, _ := os.Open(file)
	reader := csv.NewReader(bufio.NewReader(csvFile))

	for {
		line, err := reader.Read()

		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		latitude, _ := strconv.ParseFloat(line[0], 64)
		longitude, _ := strconv.ParseFloat(line[1], 64)
		unixTime, _ := strconv.ParseInt(line[2], 10, 64)

		points = append(points, types.Point{
			Latitude:  latitude,
			Longitude: longitude,
			Time:      time.Unix(unixTime, 0),
			Timestamp: unixTime,
		})
	}

	return
}
