// Copyright 2018 by Pawel Nowak [mail@pawelnowak.co.uk]. All rights reserved.
// A program that, given a series of points (latitude, longitude, timestamp) for a courier journey from A-B,
// will disregard potentially erroneous points.

package main

import "latlng/controllers"

func main() {
	controllers.Run()
}
