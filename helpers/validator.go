package helpers

import (
	"fmt"
	"latlng/types"
)

// LatLng constraints
const (
	latitudeMax  = 90.0
	latitudeMin  = -90.0
	longitudeMax = 180.0
	longitudeMin = -180.0
)

var assessedSuspects []int64

// Debug only, used to flag potentially invalid points
var suspects = [...]int64{1326379271, 1326379365, 1326379585, 1326380144, 1326380169, 1326380295}

// Flags potentially suspeced points for further debugging
func IsSuspect(unixTime int64) bool {
	for _, suspect := range suspects {
		if suspect == unixTime {
			return true
		}
	}
	return false
}

// Basic latlng point validation.
// The latitude must be a number between -90 and 90 and the longitude between -180 and 180.
func IsValidLatLng(latitude, longitude float64) (valid bool) {
	if latitude > latitudeMin && latitude < latitudeMax {
		if (longitude > longitudeMin) && (longitude < longitudeMax) {
			valid = true
		}
	}
	return
}

// ValidatePoints evaluates all points by distance, since timestamp seems to be deceiving...
// There are points with zero or tiny distance between them but larger time difference than more distant points.
func ValidatePoints(points []types.Point, distanceThreshold float64) (results []types.Point, debugMsg []string) {

	var msg string

START:
	for idx, point := range points {

		if idx < 1 {
			continue
		}

		point.TimeDiff = point.TimeToPoint(points[idx-1])
		point.Distance = point.DistanceToPoint(points[idx-1])
		point.Suspect = IsSuspect(point.Timestamp)
		point.Valid = IsValidLatLng(point.Latitude, point.Longitude)

		if point.Distance <= distanceThreshold {
			msg = fmt.Sprintf("%d %s", idx, point)
			debugMsg = append(debugMsg, msg)
			results = append(results, point)
			continue
		}

		// Point after suspect will have skewed distance and time to point
		// Fix it by calculating distance and time between last correct point before suspect and current point
		for _, s := range assessedSuspects {
			if s == points[idx-1].Timestamp {
				d := 0.00
				t := "00:00:00"

				if len(results) > 0 {
					lastCorrectPoint := results[len(results)-1]
					d = point.DistanceToPoint(lastCorrectPoint)
					t = point.TimeToPoint(lastCorrectPoint)
				}

				point.Distance = d
				point.TimeDiff = t
				msg = fmt.Sprintf("%d %s", idx, point)
				debugMsg = append(debugMsg, msg)
				results = append(results, point)
				continue START
			}
		}

		if idx < len(points) {
			distanceBetweenPointBeforeAndAfter := points[idx-1].DistanceToPoint(points[idx+1])
			assessedSuspects = append(assessedSuspects, point.Timestamp)
			msg = fmt.Sprintf("\n%d SUSPECT: %v. %v. Distance between points before:[%v] to after:[%v] equals %.2fkm\n", idx, point.Timestamp, point, points[idx-1].Timestamp, points[idx+1].Timestamp, distanceBetweenPointBeforeAndAfter)
			debugMsg = append(debugMsg, msg)
		} else {
			msg = fmt.Sprintf("\n%d SUSPECT: %v. %v. Distance to the next point could not be calculated since no more items.\n", idx, point.Timestamp, point)
			debugMsg = append(debugMsg, msg)
		}
	}

	return
}
