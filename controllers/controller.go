package controllers

import (
	"latlng/helpers"
	"latlng/models"
	"log"
	"fmt"
)

const distanceThreshold = 0.4

func Run() {
	// Retrieve all CSV data as latlng Points
	points, err := models.GetData("points.csv")
	if err != nil {
		log.Fatal(err)
	}

	// validate all latlng points
	validPoints, debugMsg := helpers.ValidatePoints(points, distanceThreshold)

	fmt.Printf("--- FILTERED RESULTS ---\n")

	for _, point := range validPoints {
		fmt.Printf("[%v, %v, %v]\n", point.Latitude, point.Longitude, point.Timestamp)
	}

	fmt.Printf("\n--- DEBUG MESSAGE ---\n")

	for _, msg := range debugMsg {
		fmt.Println(msg)
	}

	//validPoints, _ := helpers.ValidatePoints(points, distanceThreshold)
	//json, _ := json.Marshal(validPoints)
	//fmt.Println(string(json))
}
