## LMLT Technical Test

Develop a program that, given a series of points (latitude, longitude, timestamp) for a courier journey from A-B, will disregard potentially erroneous points.

## Solution

This program filters out invalid latlng points based on distance proximity and outputs them to console.

Below filtered results, output lists debugging messages to showcase the approach and logic. 

Timestamp evaluation proved to be deceiving - there are points with zero or tiny distance between them (see debug message output) but larger time difference than between more distant points.

Program does not rely on any external libraries.

## Design

Despite small size program arrangement tries to follow MVC structure, to easily provide namespace and room for expansion without cluttering.

## Tests

Example tests are provided. These are by no means exhaustive, but should illustrate the capability.

Tests are arranged with subtests to allow running all at once, or isolated cases if needed.

## Installation & commands

Repository contains compiled binary file (latlng) compiled for linux environment.

Executing this file should run the program.

#### Run program
```
$ ./latlng

```

#### Run all tests
```
$ go test -v

```

#### Run specific  test
```
go test -v -run TestIsValidLatLng/TestIsValidLatLng/Manchester

```