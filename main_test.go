package main

import (
	"latlng/helpers"
	"testing"
)

func TestIsValidLatLng(t *testing.T) {
	tt := []struct {
		name      string
		latitude  float64
		longitude float64
		expected     bool
	}{
		{"London", 51.5074, 0.1278, true},
		{"Birmingham", 52.4862, 1.8904, true},
		{"Bristol", 51.4545, 2.5879, true},
		{"Manchester", 53.4808, 2.2426, true},
		{"England", 52.3555, 1.1743, true},
		{"latitude out of range -90 and 90", -91.000000, 180.000000, false},
		{"longitude out of range -180 and 180", 00.000000, 181.000000, false},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			if received := helpers.IsValidLatLng(tc.latitude, tc.longitude); received != tc.expected {
				t.Fatalf("Invalid latlng - %v, expected %v, received: %v", tc.name, tc.expected, received)
			}
		})
	}
}

func TestIsSuspect(t *testing.T) {
	tt := []struct {
		name      string
		timestamp int64
		expected     bool
	}{
		// suspected invalid points
		{"Suspect:1326379271", 1326379271, true},
		{"Suspect:1326379365", 1326379365, true},
		{"Suspect:1326379585", 1326379585, true},
		{"Suspect:1326380144", 1326380144, true},
		{"Suspect:1326380169", 1326380169, true},
		{"Suspect:1326380295", 1326380295, true},
		// valid points
		{"Valid:1326378718", 1326378718, false},
		{"Valid:1326378723", 1326378723, false},
		{"Valid:1326378778", 1326378778, false},
		{"Valid:1326378932", 1326378932, false},
		{"Valid:1326379103", 1326379103, false},
		{"Valid:1326379231", 1326379231, false},
		{"Valid:1326379376", 1326379376, false},
		{"Valid:1326379917", 1326379917, false},
		{"Valid:1326380102", 1326380102, false},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			if received := helpers.IsSuspect(tc.timestamp); received != tc.expected {
				t.Fatalf("%v expected to be %v, received: %v", tc.name, tc.expected, received)
			}
		})
	}
}
