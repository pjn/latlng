package types

import (
	"fmt"
	"math"
	"strconv"
	"time"
)

type Point struct {
	Latitude  float64   `json:"latitude"`
	Longitude float64   `json:"longitude"`
	Time      time.Time `json:"time"`
	Timestamp int64     `json:"timestamp"`
	Valid     bool      `json:"valid"`
	Suspect   bool      `json:"suspect"`
	TimeDiff  string    `json:"timediff"`
	Distance  float64   `json:"distance"`
}

func (p Point) String() string {
	return fmt.Sprintf("[Latitude: %f, Longitude: %f, Time: %s, Timestamp: %v, Valid %v, Suspect: %v, TimeDiff: %s, Distance: %.2fkm]",
		p.Latitude, p.Longitude, p.Time, p.Timestamp, p.Valid, p.Suspect, p.TimeDiff, p.Distance)
}

func (p Point) DistanceToPoint(point Point) float64 {

	if p.Latitude == point.Latitude && p.Longitude == point.Longitude {
		return 0.00
	}

	// Radius of the earth in km
	const R = 6371.0

	dLat := deg2rad(point.Latitude - p.Latitude)
	dLon := deg2rad(point.Longitude - p.Longitude)
	a := math.Sin(dLat/2)*math.Sin(dLat/2) + math.Cos(deg2rad(p.Latitude))*math.Cos(deg2rad(point.Latitude))*math.Sin(dLon/2)*math.Sin(dLon/2)
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	d := R * c // Distance in km
	distance, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", d), 64)
	return distance
}

func deg2rad(deg float64) float64 {
	return deg * (math.Pi / 180)
}

func (p Point) TimeToPoint(point Point) string {
	diff := p.Time.Sub(point.Time)
	out := time.Time{}.Add(diff)
	return out.Format("15:04:05")
}
